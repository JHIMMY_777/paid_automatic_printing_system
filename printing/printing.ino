/*********************************************************************
* TITLE: Paid automated printing system
* AUTHOR: Jhimmy Astoraque Duran
* DESCRIPTION: This a project that uses a conventional home printer to be able
               to accept coins for printing either gray scale or color pages for commercial purposes.
               Materials: Arduino, Coin acceptor, LCD, wires, buttons, servomotors, proximity sensor, etc.
* VERSION: 0.1b
*********************************************************************/

#include <LiquidCrystal.h>
#include <Servo.h>

#define COIN_VALUE 0.5
#define NO_PAPER_ANALOG_SET 550
#define PAPER_ANALOG_SET 100


const int rs = 8, en = 9, d4 = 4, d5 = 5, d6 = 6, d7 = 7;
LiquidCrystal lcd(rs, en, d4, d5, d6, d7);

Servo servoPower;
Servo servoStop;

const byte coinAcceptorPin = 2;
const byte colorBtnPin = 12;
const byte blackBtnPin = 13;
const byte servoPowerPin = 10;
const byte servoStopPin = 11;
const byte IRSensorPin = A0;

uint16_t analogValueIR;
bool pageStart = false; // to reference when IR detects the beginning of a printing
bool pageEnd = false; // to reference when IR detects the ending of a printing

byte blackPages = 0;
float credit = 0.0;
bool creditChange = true; // to control if changes and update LCD
bool printerOn = false;

void setup() {
  Serial.begin(230400);

  servoPower.attach(servoPowerPin); // will be number 0
  servoPower.write(80); // starting angle
  delay(250);
  servoStop.attach(servoStopPin); // will be number 1
  servoStop.write(72);

  // set up the LCD's number of columns and rows:
  lcd.begin(16, 2);
  lcd.clear();
  // Print a welcome message to the LCD.
  lcd.print("BIENVENIDOS A");
  lcd.setCursor(0, 1);
  lcd.print("EASY PRINT!");
  delay(1000);
  lcd.clear();
  delay(1000);
  lcd.print("BIENVENIDOS A");
  lcd.setCursor(0, 1);
  lcd.print("EASY PRINT!");
  delay(1000);
  lcd.clear();
  delay(700);


} // EOF setup

void loop() {
  pollingForCoin();
  calculatePossiblePagesToPrint(credit);
  if (creditChange){
    lcdMainMenu(credit, blackPages);
  }
  turnOnPrinterOnMinimumCredit(COIN_VALUE);
  
  // experimental automatic detection of printing start
  readIRSensor();
  if (credit != 0 && paperChecker() == 1){
    lcdPrintTime();
    printingSequence(blackPages); // print # pages
    credit = 0.0; // credit 0 and pages will be zero too
    creditChange = true;
    Serial.println("He salido");
  }
  delay(7);
} // EOF loop

void pollingForCoin(){
  if (digitalRead(coinAcceptorPin) == HIGH){
    Serial.println("A coin is Here!!");
    credit += COIN_VALUE;
    creditChange = true;
    delay(100);
  }
} // EOF pollingForCoin


void calculatePossiblePagesToPrint(float cred){ 
  String strCredit = (String) cred;
  String wholeDigit = (String)strCredit[0];
  String decimalDigit = (String)strCredit[2];
  int pages = wholeDigit.toInt() * 5;
  if (decimalDigit.toInt() != 0){
    pages += 2;
  }
  blackPages = pages;
} // EOF calculatePages


void lcdMainMenu(float cred, byte pages){
  // updates the credit and pages to print in LCD
  lcd.clear();
  lcd.home();
  lcd.print("CREDITO: ");
  lcd.print(cred);
  lcd.print(" Bs.");
  lcd.setCursor(0, 1);
  lcd.print("PAGINAS: ");
  lcd.print(pages);
  creditChange = false;
} // lcdMainMenu

void lcdPrintTime(){
  // updates the credit and pages to print in LCD
  lcd.clear();
  lcd.home();
  lcd.print("IMPRIMIENDO !!");
  lcd.setCursor(0, 1);
  lcd.print("ESPERE ...");
} // lcdMainMenu

void turnOnPrinterOnMinimumCredit(float minimum){
  if (credit >= minimum && printerOn == false){
    servoPressButton(0); // turn on printer
    Serial.println("Turn Printer On jadsacorp");
    printerOn = true;
  }
} // EOF turnOnPrinterOnMinimumCredit

void readIRSensor(){
  // this func will store the analog read current IR value to variable analogValueIR
  analogValueIR = analogRead(IRSensorPin);
} // EOF readIRSensor

int paperChecker(){
  // returns true if paper detected, otherwise returns false
  if (analogValueIR >= NO_PAPER_ANALOG_SET){
    return 0;
  }
  else if (analogValueIR <= PAPER_ANALOG_SET){
    return 1;
  }
  else{
    return -1;  // neither cold nor hot
  }
} // EOF paperChecker


void printingSequence(byte numberOfPages){

  for(byte i = 0; i < numberOfPages; i++)
  {
    while (true){
      // check tray empty it is empty tray for now
      readIRSensor();
      if (paperChecker() == 1){
        Serial.println("Primer se detecta cabeza de hoja");
        break;
      }
      delay(5);//50
    }

    int counter = 0;
    while (true){
      // if (i + 1 == numberOfPages){
      //   break;
      // }
      // check tray empty it is empty tray for now
      counter++;
      Serial.println(counter);
      if (counter >= 880 && i + 1 == numberOfPages ){
        break;
      }
      readIRSensor();
      if (paperChecker() == 0){
        Serial.println("Segundo se detecta fin de hoja");
        break;
      }
      delay(5); //50
    }
    // fin de ciclo de hoja, hacer esto las hojas q sean necesarias
    Serial.print(">>>>>>>>>>>>>>> ");
    Serial.print(i + 1);
    Serial.println(" Hoja(s) imprimida hasta el momento <<<<<<<<<<");
  } // eof for
  servoPressButton(1); //Stop the printer servomanually
  Serial.println("Presionar el Boton Stop con servo!");
  delay(3500);
  servoPressButton(0); //Stop the printer servomanually
  Serial.println("Presionar APAGADO IMPRESORA!");
  printerOn = false;
  delay(7000); // waiting time before more coins
} // EOF printingSequence

void servoPressButton(int servoNumber){
  const byte angleSet = 80; // degrees
  const byte speedSet = 160; // ms
  if (servoNumber == 0){
    servoPower.write(52);
    delay(speedSet);
    servoPower.write(angleSet+10);
    delay(10);
    servoPower.write(angleSet);
  }
  else if (servoNumber == 1){
    servoStop.write(47);
    delay(speedSet);
    servoPower.write(angleSet+10);
    delay(10);
    servoStop.write(72);
  }
} // EOF servoPressButton